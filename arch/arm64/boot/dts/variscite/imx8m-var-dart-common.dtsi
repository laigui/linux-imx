/*
 * Copyright 2017 NXP
 * Copyright 2018 Variscite Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/dts-v1/;

#include "../freescale/fsl-imx8mq.dtsi"

/ {
	compatible = "variscite,imx8m-dart","fsl,imx8mq";

	chosen {
		bootargs = "console=ttymxc0,115200 earlycon=ec_imx6q,0x30860000,115200";
		stdout-path = &uart1;
	};

	regulators {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;

		reg_audio: audio_vdd {
			compatible = "regulator-fixed";
			regulator-name = "wm8904_supply";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};
	};

	sound-wm8904 {
		compatible = "fsl,imx-audio-wm8904";
		model = "imx-wm8904";
		audio-cpu = <&sai3>;
		audio-codec = <&wm8904>;
		audio-routing =
			"Headphone Jack", "HPOUTL",
			"Headphone Jack", "HPOUTR",
			"IN2L", "Line In Jack",
			"IN2R", "Line In Jack",
			"IN1L", "Mic",
			"Playback", "CPU-Playback",
			"CPU-Capture", "Capture";
		status = "okay";
        };

	sound-hdmi {
		compatible = "fsl,imx-audio-cdnhdmi";
		model = "imx-audio-hdmi";
		audio-cpu = <&sai4>;
		protocol = <1>;
		status = "disabled";
	};

	sound-spdif {
		compatible = "fsl,imx-audio-spdif";
		model = "imx-spdif";
		spdif-controller = <&spdif1>;
		spdif-out;
		spdif-in;
		status = "disabled";
	};

	sound-hdmi-arc {
		compatible = "fsl,imx-audio-spdif";
		model = "imx-hdmi-arc";
		spdif-controller = <&spdif2>;
		spdif-in;
		status = "disabled";
	};

        backlight_lvds: backlight_lvds {
                compatible = "pwm-backlight";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_pwm1>;
                pwms = <&pwm1 0 1000000>;
                brightness-levels = <0 4 8 16 32 64 128 255>;
                default-brightness-level = <7>;
                status = "disabled";
        };

        gpio-keys {
                compatible = "gpio-keys";
                pinctrl-names = "default";
                pinctrl-0 = <&pinctrl_gpio_keys>;

                up {
                        label = "Up";
                        gpios = <&gpio4 18 GPIO_ACTIVE_LOW>;
                        linux,code = <KEY_UP>;
                };

                down {
                        label = "Down";
                        gpios = <&gpio4 15 GPIO_ACTIVE_LOW>;
                        linux,code = <KEY_DOWN>;
                };

                home {
                        label = "Home";
                        gpios = <&gpio4 13 GPIO_ACTIVE_LOW>;
                        linux,code = <KEY_HOME>;
                };

                back {
                        label = "Back";
                        gpios = <&gpio4 6 GPIO_ACTIVE_LOW>;
                        linux,code = <KEY_BACK>;
                };
        };

	leds {
		compatible = "gpio-leds";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_leds>;
	
		emmc {
			label = "eMMC";
			gpios = <&gpio4 17 GPIO_ACTIVE_HIGH>;
			linux,default-trigger = "mmc0";
		};
	};
};

&clk {
	assigned-clocks = <&clk IMX8MQ_AUDIO_PLL1>;
	assigned-clock-rates = <786432000>;
};

&iomuxc {
	pinctrl-names = "default";

	imx8mq-evk {

		pinctrl_csi1: csi1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXD6_GPIO4_IO8		0x19
				MX8MQ_IOMUXC_UART4_RXD_GPIO5_IO28		0x19
			>;
		};

		pinctrl_csi2: csi2grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXD7_GPIO4_IO9		0x19
				MX8MQ_IOMUXC_UART4_TXD_GPIO5_IO29		0x19
			>;
		};

		pinctrl_fec1: fec1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_ENET_MDC_ENET1_MDC			0x3
				MX8MQ_IOMUXC_ENET_MDIO_ENET1_MDIO		0x23
				MX8MQ_IOMUXC_ENET_TD3_ENET1_RGMII_TD3		0x1f
				MX8MQ_IOMUXC_ENET_TD2_ENET1_RGMII_TD2		0x1f
				MX8MQ_IOMUXC_ENET_TD1_ENET1_RGMII_TD1		0x1f
				MX8MQ_IOMUXC_ENET_TD0_ENET1_RGMII_TD0		0x1f
				MX8MQ_IOMUXC_ENET_RD3_ENET1_RGMII_RD3		0x91
				MX8MQ_IOMUXC_ENET_RD2_ENET1_RGMII_RD2		0x91
				MX8MQ_IOMUXC_ENET_RD1_ENET1_RGMII_RD1		0x91
				MX8MQ_IOMUXC_ENET_RD0_ENET1_RGMII_RD0		0x91
				MX8MQ_IOMUXC_ENET_TXC_ENET1_RGMII_TXC		0x1f
				MX8MQ_IOMUXC_ENET_RXC_ENET1_RGMII_RXC		0x91
				MX8MQ_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x91
				MX8MQ_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x1f
				MX8MQ_IOMUXC_GPIO1_IO09_GPIO1_IO9		0x19
			>;
		};

		pinctrl_i2c1: i2c1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_I2C1_SCL_I2C1_SCL			0x4000007f
				MX8MQ_IOMUXC_I2C1_SDA_I2C1_SDA			0x4000007f
			>;
		};

		pinctrl_i2c2: i2c2grp {
			fsl,pins = <
				MX8MQ_IOMUXC_I2C2_SCL_I2C2_SCL			0x4000007f
				MX8MQ_IOMUXC_I2C2_SDA_I2C2_SDA			0x4000007f
			>;
		};

		pinctrl_i2c3: i2c3grp {
			fsl,pins = <
				MX8MQ_IOMUXC_I2C3_SCL_I2C3_SCL			0x4000007f
				MX8MQ_IOMUXC_I2C3_SDA_I2C3_SDA			0x4000007f
			>;
		};

		pinctrl_i2c4: i2c4grp {
			fsl,pins = <
				MX8MQ_IOMUXC_I2C4_SCL_I2C4_SCL			0x4000007f
				MX8MQ_IOMUXC_I2C4_SDA_I2C4_SDA			0x4000007f
			>;
		};

		pinctrl_pcie0: pcie0grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXD5_GPIO4_IO7		0x16
			>;
		};

		pinctrl_pcie1: pcie1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_TXD7_GPIO4_IO19		0x16
			>;
		};

		pinctrl_uart1: uart1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_UART1_RXD_UART1_DCE_RX		0x49
				MX8MQ_IOMUXC_UART1_TXD_UART1_DCE_TX		0x49
			>;
		};

		pinctrl_uart2: uart2grp {
			fsl,pins = <
				MX8MQ_IOMUXC_UART2_RXD_UART2_DCE_RX		0x49
				MX8MQ_IOMUXC_UART2_TXD_UART2_DCE_TX		0x49
			>;
		};

		pinctrl_uart3: uart3grp {
			fsl,pins = <
				MX8MQ_IOMUXC_UART3_RXD_UART3_DCE_RX		0x49
				MX8MQ_IOMUXC_UART3_TXD_UART3_DCE_TX		0x49
			>;
		};

		pinctrl_uart4: uart4grp {
			fsl,pins = <
				MX8MQ_IOMUXC_ECSPI2_SCLK_UART4_DCE_RX           0xc1
				MX8MQ_IOMUXC_ECSPI2_MOSI_UART4_DCE_TX           0xc1
				MX8MQ_IOMUXC_ECSPI2_MISO_UART4_DCE_CTS_B        0xc1
				MX8MQ_IOMUXC_ECSPI2_SS0_UART4_DCE_RTS_B         0xc1
			>;
		};

		pinctrl_usdhc1: usdhc1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x83
				MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xc3
				MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xc3
				MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xc3
				MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xc3
				MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xc3
				MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xc3
				MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xc3
				MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xc3
				MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xc3
				MX8MQ_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x83
				MX8MQ_IOMUXC_SD1_RESET_B_USDHC1_RESET_B		0xc1
			>;
		};

		pinctrl_usdhc1_100mhz: usdhc1grp100mhz {
			fsl,pins = <
				MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x85
				MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xc5
				MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xc5
				MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xc5
				MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xc5
				MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xc5
				MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xc5
				MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xc5
				MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xc5
				MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xc5
				MX8MQ_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x85
				MX8MQ_IOMUXC_SD1_RESET_B_USDHC1_RESET_B		0xc1
			>;
		};

		pinctrl_usdhc1_200mhz: usdhc1grp200mhz {
			fsl,pins = <
				MX8MQ_IOMUXC_SD1_CLK_USDHC1_CLK			0x87
				MX8MQ_IOMUXC_SD1_CMD_USDHC1_CMD			0xc7
				MX8MQ_IOMUXC_SD1_DATA0_USDHC1_DATA0		0xc7
				MX8MQ_IOMUXC_SD1_DATA1_USDHC1_DATA1		0xc7
				MX8MQ_IOMUXC_SD1_DATA2_USDHC1_DATA2		0xc7
				MX8MQ_IOMUXC_SD1_DATA3_USDHC1_DATA3		0xc7
				MX8MQ_IOMUXC_SD1_DATA4_USDHC1_DATA4		0xc7
				MX8MQ_IOMUXC_SD1_DATA5_USDHC1_DATA5		0xc7
				MX8MQ_IOMUXC_SD1_DATA6_USDHC1_DATA6		0xc7
				MX8MQ_IOMUXC_SD1_DATA7_USDHC1_DATA7		0xc7
				MX8MQ_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x87
				MX8MQ_IOMUXC_SD1_RESET_B_USDHC1_RESET_B		0xc1
			>;
		};

		pinctrl_usdhc2_sd: usdhc2grpsd {
			fsl,pins = <
				MX8MQ_IOMUXC_SD2_CD_B_GPIO2_IO12		0x41
				MX8MQ_IOMUXC_SD2_RESET_B_GPIO2_IO19		0x41
			>;
		};

		pinctrl_usdhc2: usdhc2grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x83
				MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xc3
				MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xc3
				MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xc3
				MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xc3
				MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xc3
			>;
		};

		pinctrl_usdhc2_100mhz: usdhc2grp100mhz {
			fsl,pins = <
				MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x85
				MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xc5
				MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xc5
				MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xc5
				MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xc5
				MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xc5
			>;
		};

		pinctrl_usdhc2_200mhz: usdhc2grp200mhz {
			fsl,pins = <
				MX8MQ_IOMUXC_SD2_CLK_USDHC2_CLK			0x87
				MX8MQ_IOMUXC_SD2_CMD_USDHC2_CMD			0xc7
				MX8MQ_IOMUXC_SD2_DATA0_USDHC2_DATA0		0xc7
				MX8MQ_IOMUXC_SD2_DATA1_USDHC2_DATA1		0xc7
				MX8MQ_IOMUXC_SD2_DATA2_USDHC2_DATA2		0xc7
				MX8MQ_IOMUXC_SD2_DATA3_USDHC2_DATA3		0xc7
			>;
		};

		pinctrl_usdhc2_vsel: usdhc2grpvsel {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO04_USDHC2_VSELECT		0xc1
			>;
		};

		pinctrl_sai3: sai3grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI3_RXFS_SAI3_RX_SYNC             0xd6
				MX8MQ_IOMUXC_SAI3_RXC_SAI3_RX_BCLK              0xd6
				MX8MQ_IOMUXC_SAI3_RXD_SAI3_RX_DATA0             0xd6
				MX8MQ_IOMUXC_SAI3_TXFS_SAI3_TX_SYNC             0xd6
				MX8MQ_IOMUXC_SAI3_TXC_SAI3_TX_BCLK              0xd6
				MX8MQ_IOMUXC_SAI3_TXD_SAI3_TX_DATA0             0xd6
				MX8MQ_IOMUXC_SAI3_MCLK_SAI3_MCLK                0xd6
			>;
		};

		pinctrl_spdif1: spdif1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_SPDIF_TX_SPDIF1_OUT		0xd6
				MX8MQ_IOMUXC_SPDIF_RX_SPDIF1_IN			0xd6
			>;
		};

		pinctrl_wdog: wdoggrp {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO02_WDOG1_WDOG_B 		0xc6
			>;
		};

		pinctrl_pwm1: pwm1grp {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO01_PWM1_OUT 		0x56
			>;
		};

		pinctrl_touch: touchgrp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXD3_GPIO4_IO5 		0x16
			>;
		};

		pinctrl_rtc: rtcgrp {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO15_GPIO1_IO15 		0xc1
			>;
		};

		pinctrl_gpio_keys: keygrp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_TXD3_GPIO4_IO15		0xc6
				MX8MQ_IOMUXC_SAI1_TXD6_GPIO4_IO18		0xc6
				MX8MQ_IOMUXC_SAI1_TXD1_GPIO4_IO13		0xc6
				MX8MQ_IOMUXC_SAI1_RXD4_GPIO4_IO6		0xc6
			>;
		};

		pinctrl_leds: ledgrp {
			fsl,pins = <
				MX8MQ_IOMUXC_SAI1_RXD1_GPIO4_IO3		0xc6
				MX8MQ_IOMUXC_SAI1_TXD2_GPIO4_IO14		0xc6
				MX8MQ_IOMUXC_SAI1_RXD2_GPIO4_IO4		0xc6
				MX8MQ_IOMUXC_SAI1_TXD5_GPIO4_IO17		0xc6
			>;
		};

		pinctrl_wifi: wifigrp {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO04_GPIO1_IO4		0xc1    /* WIFI_PWR_VSEL */
				MX8MQ_IOMUXC_GPIO1_IO08_GPIO1_IO8		0xc1    /* WIFI_PWR_ON   */
				MX8MQ_IOMUXC_NAND_CE1_B_GPIO3_IO2		0xc1    /* WIFI_REG_ON   */
				MX8MQ_IOMUXC_NAND_CE3_B_GPIO3_IO4		0xc1    /* BT_REG_ON     */
   				MX8MQ_IOMUXC_SPDIF_EXT_CLK_GPIO5_IO5		0xc1    /* BT_BUF_EN     */  
			>;
		};

		pinctrl_ethphy: ethphygrp {
			fsl,pins = <
				MX8MQ_IOMUXC_GPIO1_IO08_GPIO1_IO8		0xc1
			>;
		};
	};
};

&fec1 {
	phy-mode = "rgmii";
	phy-handle = <&ethphy0>;
	fsl,magic-packet;
	status = "okay";
	phy-reset-gpios = <&gpio1 9 GPIO_ACTIVE_LOW>;
	phy-reset-duration = <10>;

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@0 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <0>;
			at803x,led-act-blind-workaround;
			at803x,eee-disabled;
		};
	};
};

&i2c1 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";

	pmic: pfuze100@08 {
		compatible = "fsl,pfuze100";
		reg = <0x08>;

		regulators {
			sw1a_reg: sw1ab {
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1875000>;
			};

			sw1c_reg: sw1c {
				regulator-min-microvolt = <300000>;
				regulator-max-microvolt = <1875000>;
			};

			sw2_reg: sw2 {
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			sw3a_reg: sw3ab {
				regulator-min-microvolt = <400000>;
				regulator-max-microvolt = <1975000>;
				regulator-always-on;
			};

			sw4_reg: sw4 {
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			swbst_reg: swbst {
				regulator-min-microvolt = <5000000>;
				regulator-max-microvolt = <5150000>;
			};

			snvs_reg: vsnvs {
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <3000000>;
				regulator-always-on;
			};

			vref_reg: vrefddr {
				regulator-always-on;
			};

			vgen1_reg: vgen1 {
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1550000>;
			};

			vgen2_reg: vgen2 {
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1550000>;
				regulator-always-on;
			};

			vgen3_reg: vgen3 {
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vgen4_reg: vgen4 {
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vgen5_reg: vgen5 {
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vgen6_reg: vgen6 {
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
			};
		};
	};

	wm8904: codec@1a {
		compatible = "wlf,wm8904";
		reg = <0x1a>;
		clocks = <&clk IMX8MQ_CLK_SAI3_ROOT>;
		clock-names = "mclk";
		DCVDD-supply = <&reg_audio>;
		DBVDD-supply = <&reg_audio>;
		AVDD-supply = <&reg_audio>;
		CPVDD-supply = <&reg_audio>;
		MICVDD-supply = <&reg_audio>;
		gpio-cfg = <
			0x0018 /* GPIO1 => DMIC_CLK */
			0xffff /* GPIO2 => don't touch */
			0xffff /* GPIO3 => don't touch */
			0xffff /* GPIO4 => don't touch */
		>;
		status = "okay";
	};

	dsi_lvds_bridge: sn65dsi84@2c {
		reg = <0x2c>;
		status = "disabled";
		compatible = "ti,sn65dsi84";
		enable-gpios = <&gpio1 11 GPIO_ACTIVE_HIGH>;

		sn65dsi84,addresses = <	0x09 0x0A 0x0B 0x0D 0x10 0x11 0x12 0x13
					0x18 0x19 0x1A 0x1B 0x20 0x21 0x22 0x23
					0x24 0x25 0x26 0x27 0x28 0x29 0x2A 0x2B
					0x2C 0x2D 0x2E 0x2F 0x30 0x31 0x32 0x33
					0x34 0x35 0x36 0x37 0x38 0x39 0x3A 0x3B
					0x3C 0x3D 0x3E 0x0D>;

		sn65dsi84,values =    <	0x00 0x01 0x10 0x00 0x26 0x00 0x11 0x00
					0x7a 0x00 0x03 0x00 0x20 0x03 0x00 0x00
					0x00 0x00 0x00 0x00 0x21 0x00 0x00 0x00
					0x30 0x00 0x00 0x00 0x03 0x00 0x00 0x00
					0x28 0x00 0x00 0x00 0x00 0x00 0x00 0x00
					0x00 0x00 0x00 0x01>;
	};
};

&i2c2 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c2>;
	status = "okay";

        /* DS1337 RTC module */
	rtc@0x68 {
		status = "okay";
		compatible = "dallas,ds1337";
		reg = <0x68>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_rtc>;
		interrupt-parent = <&gpio1>;
		interrupts = <15 IRQ_TYPE_EDGE_FALLING>;
        };

	/* Capacitive touch controller */
	ft5x06_ts: ft5x06_ts@38 {
		status = "disabled";
		compatible = "edt,edt-ft5x06";
		reg = <0x38>;
		reset-gpios = <&gpio4 5 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_touch>;
		interrupt-parent = <&gpio1>;
		interrupts = <14 0>;
		touchscreen-size-x = <800>;
		touchscreen-size-y = <480>;
		touchscreen-inverted-x;
		touchscreen-inverted-y;
        };

	/* USB-C controller */
	typec_ptn5150: typec@3d {
		status = "okay";
		compatible = "nxp,ptn5150";
		reg = <0x3d>;
		connect-gpios = <&gpio1 10 GPIO_ACTIVE_HIGH>;
	};

	ov5640_mipi: ov5640_mipi@3c {
		status = "okay";
		compatible = "ovti,ov5640_mipi";
		reg = <0x3c>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_csi1>;
		clocks = <&clk IMX8MQ_CLK_CLKO2_DIV>;
		clock-names = "csi_mclk";
/* Disabled CLKO2, since DART-MX8M camera expansion board uses
 * its own oscillator. Enable CLK02 if your desing requres it
 */
#if 0
		assigned-clocks = <&clk IMX8MQ_CLK_CLKO2_SRC>,
				  <&clk IMX8MQ_CLK_CLKO2_DIV>;
		assigned-clock-parents = <&clk IMX8MQ_SYS2_PLL_200M>;
		assigned-clock-rates = <0>, <24000000>;
#endif
		csi_id = <0>;
		pwn-gpios = <&gpio4 8 GPIO_ACTIVE_HIGH>;
		rst-gpios = <&gpio5 28 GPIO_ACTIVE_HIGH>;
		mclk = <24000000>;
		mclk_source = <0>;
		port {
			ov5640_mipi1_ep: endpoint {
				remote-endpoint = <&mipi1_sensor_ep>;
			};
		};
	};
};

&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c3>;
	status = "okay";
};

&i2c4 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c4>;
	status = "okay";

	ov5640_mipi2: ov5640_mipi2@3c {
		status = "okay";
		compatible = "ovti,ov5640_mipi";
		reg = <0x3c>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_csi2>;

		clocks = <&clk IMX8MQ_CLK_CLKO2_DIV>;
		clock-names = "csi_mclk";
/* Disabled CLKO2, since DART-MX8M camera expansion board uses
 * its own oscillator. Enable CLK02 if your desing requres it
 */
#if 0
		assigned-clocks = <&clk IMX8MQ_CLK_CLKO2_SRC>,
				  <&clk IMX8MQ_CLK_CLKO2_DIV>;
		assigned-clock-parents = <&clk IMX8MQ_SYS2_PLL_200M>;
		assigned-clock-rates = <0>, <24000000>;
#endif
		csi_id = <1>;
		pwn-gpios = <&gpio4 9 GPIO_ACTIVE_HIGH>;
		rst-gpios = <&gpio5 29 GPIO_ACTIVE_HIGH>;
		mclk = <24000000>;
		mclk_source = <0>;
		port {
			ov5640_mipi2_ep: endpoint {
				remote-endpoint = <&mipi2_sensor_ep>;
			};
		};
	};
};

&pcie0 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie0>;
	reset-gpio = <&gpio4 7 GPIO_ACTIVE_LOW>;
	ext_osc = <1>;
	status = "okay";
};

&pcie1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie1>;
	reset-gpio = <&gpio4 19 GPIO_ACTIVE_LOW>;
	ext_osc = <1>;
	status = "okay";
};

/* Console */
&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART1_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};

/* Header */
&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART2_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};

/* Header */
&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART3_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_CLK_25M>;
	status = "okay";
};

/* Bluetooth */
&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4>;
	assigned-clocks = <&clk IMX8MQ_CLK_UART4_SRC>;
	assigned-clock-parents = <&clk IMX8MQ_SYS1_PLL_80M>;
	fsl,uart-has-rtscts;
	status = "disabled";
};

/* eMMC */
&usdhc1 {
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc1>;
	pinctrl-1 = <&pinctrl_usdhc1_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc1_200mhz>;
	bus-width = <8>;
	non-removable;
	status = "okay";
};

&usb3_phy0 {
	status = "okay";
};

&usb3_0 {
	status = "okay";
};

&usb_dwc3_0 {
	status = "okay";
	extcon = <&typec_ptn5150>;
	dr_mode = "otg";
};

&usb3_phy1 {
	status = "okay";
};

&usb3_1 {
	status = "okay";
};

&usb_dwc3_1 {
	status = "okay";
	dr_mode = "host";
};

&sai3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai3>;
	assigned-clocks = <&clk IMX8MQ_CLK_SAI3_SRC>,
			<&clk IMX8MQ_CLK_SAI3_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <24576000>;
	status = "okay";
};

&sai4 {
	assigned-clocks = <&clk IMX8MQ_CLK_SAI4_SRC>,
			<&clk IMX8MQ_CLK_SAI4_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <24576000>;
	status = "disabled";
};

&spdif1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_spdif1>;
	assigned-clocks = <&clk IMX8MQ_CLK_SPDIF1_SRC>,
			<&clk IMX8MQ_CLK_SPDIF1_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <24576000>;
	status = "disabled";
};

&spdif2 {
	assigned-clocks = <&clk IMX8MQ_CLK_SPDIF2_SRC>,
			<&clk IMX8MQ_CLK_SPDIF2_DIV>;
	assigned-clock-parents = <&clk IMX8MQ_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <0>, <24576000>;
	status = "disabled";
};

&gpu_pd {
	power-supply = <&sw1a_reg>;
};

&vpu_pd {
	power-supply = <&sw1c_reg>;
};

&gpu {
	status = "okay";
};

&vpu {
	status = "okay";
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,wdog_b;
	status = "okay";
};

&mu {
	status = "okay";
};

&rpmsg{
	/*
	 * 64K for one rpmsg instance:
	 * --0xb8000000~0xb800ffff: pingpong
	 */
	vdev-nums = <1>;
	reg = <0x0 0xb8000000 0x0 0x10000>;
	status = "okay";
};

&A53_0 {
	operating-points = <
		/* kHz    uV */
		1500000 1000000
		1300000 1000000
		1000000 900000
		800000  900000
	>;
};

&csi1_bridge {
	fsl,mipi-mode;
	fsl,two-8bit-sensor-mode;
	status = "okay";

	port {
		csi1_ep: endpoint {
			remote-endpoint = <&csi1_mipi_ep>;
		};
	};
};

&csi2_bridge {
	fsl,mipi-mode;
	fsl,two-8bit-sensor-mode;
	status = "okay";

	port {
		csi2_ep: endpoint {
			remote-endpoint = <&csi2_mipi_ep>;
		};
	};
};

&mipi_csi_1 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi1_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi1_ep>;
			data-lanes = <1 2>;
		};

		csi1_mipi_ep: endpoint2 {
			remote-endpoint = <&csi1_ep>;
		};
	};
};

&mipi_csi_2 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	port {
		mipi2_sensor_ep: endpoint1 {
			remote-endpoint = <&ov5640_mipi2_ep>;
			data-lanes = <1 2>;
		};

		csi2_mipi_ep: endpoint2 {
			remote-endpoint = <&csi2_ep>;
		};
	};
};

&mipi_dsi {
	status = "disabled";
	assigned-clocks = <&clk IMX8MQ_CLK_DSI_PHY_REF_SRC>,
			  <&clk IMX8MQ_CLK_DSI_CORE_SRC>,
			  <&clk IMX8MQ_VIDEO_PLL1_REF_SEL>,
			  <&clk IMX8MQ_VIDEO_PLL1>;
	assigned-clock-parents = <&clk IMX8MQ_VIDEO_PLL1_OUT>,
				 <&clk IMX8MQ_SYS1_PLL_266M>,
				 <&clk IMX8MQ_CLK_25M>;
	assigned-clock-rates = <24000000>,
			       <266000000>,
			       <0>,
			       <599999999>;
};

&mipi_dsi_bridge {
	status = "disabled";

	panel@0 {
                reg = <0>;
                status = "okay";
                compatible = "sgd,gktw70sdae4sd";
                backlight = <&backlight_lvds>;

		dsi-lanes = <4>;
		panel-width-mm  = <800>;
		panel-height-mm = <480>;

		port {
			panel_in: endpoint {
				remote-endpoint = <&mipi_dsi_bridge_out>;
			};
		};
	};

	port@1 {
		mipi_dsi_bridge_out: endpoint {
			remote-endpoint = <&panel_in>;
		};
	};
};

&snvs_rtc {
	status = "disabled";
};

